package mataDra.logics.battles;

import java.util.List;

import mataDra.entity.creatures.Character;

public class MultiAttackNormal extends AttackNormalLogic {

    public void MultiAttack(Character oc, int abilityIndex, List<Character> dcs){
        setAttack(oc, abilityIndex, dcs);
    }

}
