package mataDra.logics.items;

import java.util.ArrayList;
import java.util.List;

public abstract class ItemListLogic<T> {

	private List<Integer> itemList = new ArrayList<>();

	//番号で各種アイテム取得
	public abstract T getItem(int index);

	//アイテム番号の追加
	public void addItem(int index){
		itemList.add(index);
	}
	//アイテム番号の削除
	public void removeItem(int index){
		itemList.remove(index);
	}
}
